/* Scalar multiplication: C[i] = A[i] * B[i].
 * Device code.
 */
 
// OpenCL Kernel
__kernel void
scalar_multiply(__global float* C, 
          __global float* A, 
          __global float* B)
{
   int i = get_global_id(0); 
    C[i] = A[i] * B[i];
}
