#include <stdio.h>
#include <string.h>

#define N_ROWS 64

extern void scalar_multiply(volatile float *A, volatile float *B, volatile float *C,
		unsigned int offset_x, unsigned int offset_y, unsigned int offset_z);

void scalar_multiply_sw(volatile float *A, volatile float *B, volatile float *C,
        unsigned int offset_x, unsigned int offset_y, unsigned int offset_z)
{
    for( size_t i=0; i<N_ROWS; i++ ) {
        C[i] = A[i] * B[i];
    }
}

int main() {
	printf("Running Sim...\n");
	float vec_a[N_ROWS];
	float vec_b[N_ROWS];
	float vec_c[N_ROWS];
    float vec_a_sw[N_ROWS];
    float vec_b_sw[N_ROWS];
    float vec_c_sw[N_ROWS];
	int i;
	for( i=0; i<N_ROWS; i++ ) {
        vec_a[i] = i;
        vec_a_sw[i] = i;
        vec_b[i] = N_ROWS - i;
        vec_b_sw[i] = N_ROWS - i;
        vec_c[i] = 0;
        vec_c_sw[i] = 0;
	}
	scalar_multiply( vec_a, vec_b, vec_c, 0, 0, 0);
    scalar_multiply_sw( vec_a, vec_b, vec_c, 0, 0, 0);  

    // Validate Results accuracy
    float diff;    
    for( i=0; i<N_ROWS; i++ ) {
        diff = (( vec_c_sw[i] > vec_c[i]) ? vec_c_sw[i] - vec_c[i] : vec_c[i] - vec_c_sw[i]);
        //printf("Row %d difference: %f \n", i, diff);
    }
}
