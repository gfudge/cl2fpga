#define WG_SIZE_Z 8
#define WG_SIZE_Y 8
#define WG_SIZE_X 8
#define BRAM_SIZE 512

//#define <string.h>

// OpenCL Kernel Matrix Multiplication test case
// ALL input arrays (includeing C) must be initialised
 void
threedim_matmul(volatile float *if_A, volatile float *if_B, volatile float *if_C, unsigned int offset_x, unsigned int offset_y, unsigned int offset_z)
{
    // Matrix height, width dimensions
    #pragma HLS INTERFACE m_axi port=if_A offset=slave bundle=gmem depth=512
#pragma HLS INTERFACE m_axi port=if_B offset=slave bundle=gmem depth=512
#pragma HLS INTERFACE m_axi port=if_C offset=slave bundle=gmem depth=512
#pragma HLS INTERFACE s_axilite port=if_A bundle=control    
#pragma HLS INTERFACE s_axilite port=if_B bundle=control    
#pragma HLS INTERFACE s_axilite port=if_C bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_x bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_y bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_z bundle=control    
#pragma HLS INTERFACE s_axilite port=return bundle=control 

float A[BRAM_SIZE];
float B[BRAM_SIZE];
float C[BRAM_SIZE];
memcpy(A, (float *) if_A, BRAM_SIZE * sizeof(float) );
memcpy(B, (float *) if_B, BRAM_SIZE * sizeof(float) );
memcpy(C, (float *) if_C, BRAM_SIZE * sizeof(float) );

for( unsigned int WorkGroup_z=0; WorkGroup_z<WG_SIZE_Z; WorkGroup_z++ ) { 
for( unsigned int WorkGroup_y=0; WorkGroup_y<WG_SIZE_Y; WorkGroup_y++ ) { 
for( unsigned int WorkGroup_x=0; WorkGroup_x<WG_SIZE_X; WorkGroup_x++ ) { 

	int wA, hA, wB, hB;
    wA = 4;    
    wB = 4;
    hA = 4;
    hB = 4;

    // Result matrix size is height hA, width wB
    int i = WorkGroup_x + offset_x;
    int j = WorkGroup_y + offset_y;
    int k = WorkGroup_z + offset_z;

    // Compute element of multiplication
    C[i*hA + j] += A[i*hA + k] * B[k*wB + j];

}
}
}

memcpy( (float *) if_C, C, BRAM_SIZE * sizeof(float) );
}
