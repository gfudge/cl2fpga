// OpenCL Kernel Matrix Multiplication test case
// ALL input arrays (includeing C) must be initialised
__kernel void
threedim_matmul(__global float* A, __global float* B, __global float* C)
{
    // Matrix height, width dimensions
    int wA, hA, wB, hB;
    wA = 4;    
    wB = 4;
    hA = 4;
    hB = 4;

    // Result matrix size is height hA, width wB
    int i = get_global_id(0);
    int j = get_global_id(1);
    int k = get_global_id(2);

    // Compute element of multiplication
    C[i*hA + j] += A[i*hA + k] * B[k*wB + j];
}
