#include <stdio.h>
#include <string.h>

#define N_ROWS 4
#define N_COLS 4
#define DEPTH N_ROWS*N_COLS
extern void matrixMul(volatile float *A, volatile float *B, volatile float *C,
		unsigned int offset_x, unsigned int offset_y, unsigned int offset_z);

void testMatMul( float *A, float *B, float *C) {
    float value = 0;	
	for( int x=0; x<N_COLS; x++ ) {
		for( int y=0; y<N_ROWS; y++ ) {
			for( int k=0; k<N_COLS; k++) {
				float elem_a = A[y*N_COLS + k];
				float elem_b = B[k*N_COLS + x];
				value += elem_a * elem_b;		
			}
		}	
	}
}

void printMatrix(float mat[DEPTH]) {
	int i,j;
	for( i=0; i<N_ROWS; i++ ) {
		for( j=0; j<N_COLS; j++ ) {
			printf("%f ", mat[i*N_COLS + j]);
		}
		printf("\n");
	}
}

void zeroMatrix(float mat[DEPTH]) {
    int i,j;
    for( i=0; i<N_ROWS; i++ ) {
        for( j=0; j<N_COLS; j++ ) {
            mat[i*N_COLS + j] = 0.0;
        }
    }
}

int main() {
	printf("Running Sim...\n");
	float mat_a[DEPTH];
	float mat_b[DEPTH];
	float mat_c[DEPTH];
    zeroMatrix( mat_a );
    zeroMatrix( mat_b );
    zeroMatrix( mat_c );
	int i,j;
	for( i=0; i<N_ROWS; i++ ) {
		for( j=0; j<N_COLS; j++ ) {
			mat_a[i*N_COLS + j] = i*j;
		}
	}
	for( i=0; i<N_ROWS; i++ ) {
		for( j=0; j<N_COLS; j++ ) {
			if( i == j ) {
				mat_b[i*N_COLS + j] = 1;
			}
			else {
				mat_b[i*N_COLS + j] = 0;
			}
		}
	}
	threedim_matmul( mat_a, mat_b, mat_c, 0, 0, 0 );
	printf("Result:\n");
	printMatrix(mat_c);
}
