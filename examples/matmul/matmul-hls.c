#define WG_SIZE_Z 1
#define WG_SIZE_Y 8
#define WG_SIZE_X 8
#define BRAM_SIZE 64

//#define <string.h>

/* kernel.cl 
 * Matrix multiplication: C = A * B.
 * Device code.
 */
 
// OpenCL Kernel
 void
matrixMul(volatile float *if_C, 
          volatile float *if_A, 
          volatile float *if_B, unsigned int offset_x, unsigned int offset_y, unsigned int offset_z)
{
   #pragma HLS INTERFACE m_axi port=if_A offset=slave bundle=gmem depth=64
#pragma HLS INTERFACE m_axi port=if_B offset=slave bundle=gmem depth=64
#pragma HLS INTERFACE m_axi port=if_C offset=slave bundle=gmem depth=64
#pragma HLS INTERFACE s_axilite port=if_A bundle=control    
#pragma HLS INTERFACE s_axilite port=if_B bundle=control    
#pragma HLS INTERFACE s_axilite port=if_C bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_x bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_y bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_z bundle=control    
#pragma HLS INTERFACE s_axilite port=return bundle=control 

float A[BRAM_SIZE];
float B[BRAM_SIZE];
float C[BRAM_SIZE];
memcpy(A, (float *) if_A, BRAM_SIZE * sizeof(float) );
memcpy(B, (float *) if_B, BRAM_SIZE * sizeof(float) );

for( unsigned int WorkGroup_z=0; WorkGroup_z<WG_SIZE_Z; WorkGroup_z++ ) { 
for( unsigned int WorkGroup_y=0; WorkGroup_y<WG_SIZE_Y; WorkGroup_y++ ) { 
for( unsigned int WorkGroup_x=0; WorkGroup_x<WG_SIZE_X; WorkGroup_x++ ) { 

	int wA = 8;
   int wB = 8;
   // 2D Thread ID
   // Old CUDA code
   //int tx = blockIdx.x * TILE_SIZE + threadIdx.x;
   //int ty = blockIdx.y * TILE_SIZE + threadIdx.y;
   int tx = WorkGroup_x + offset_x; 
   int ty = WorkGroup_y + offset_y;
 
   // value stores the element that is 
   // computed by the thread
   float value = 0;
   for (int k = 0; k < wA; ++k)
   {
      float elementA = A[ty * wA + k];
      float elementB = B[k * wB + tx];
      value += elementA * elementB;
   }
 
   // Write the matrix to device memory each 
   // thread writes one element
   C[ty * wA + tx] = value;

}
}
}

memcpy( (float *) if_C, C, BRAM_SIZE * sizeof(float) );
}
