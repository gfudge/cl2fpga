#include <stdio.h>
#include <string.h>

#define N_ROWS 8
#define N_COLS 8
#define DEPTH N_ROWS*N_COLS
extern void matrixMul(volatile float *A, volatile float *B, volatile float *C,
		unsigned int offset_x, unsigned int offset_y, unsigned int offset_z);

void testMatMul( float *A, float *B, float *C) {	
	for( int x=0; x<N_COLS; x++ ) {
		for( int y=0; y<N_ROWS; y++ ) {
			for( int k=0; k<N_COLS; k++) {
				float elem_a = A[y*N_COLS + k];
				float elem_b = B[k*N_COLS + x];
				value += elem_a * elem_b;		
			}
		}	
	}
}

void printMatrix(float mat[DEPTH]) {
	int i,j;
	for( i=0; i<N_ROWS; i++ ) {
		for( j=0; j<N_COLS; j++ ) {
			printf("%f\t", mat[i*N_ROWS +j]);
		}
		printf("\n");
	}
}

int main() {
	printf("Running Sim...\n");
	float mat_a[DEPTH];
	float mat_b[DEPTH];
	float mat_c[DEPTH];
	int i,j;
	for( i=0; i<N_ROWS; i++ ) {
		for( j=0; j<N_COLS; j++ ) {
			mat_a[i*N_ROWS + j] = i*j;
		}
	}
	for( i=0; i<N_ROWS; i++ ) {
		for( j=0; j<N_COLS; j++ ) {
			if( i == j ) {
				mat_b[i*N_ROWS + j] = 1;
			}
			else {
				mat_b[i*N_ROWS + j] = 0;
			}
		}
	}
	//printMatrix(mat_a);
	//printMatrix(mat_b);
	matrixMul( mat_c, mat_a, mat_b, 0, 0, 0);
	printf("Result:\n");
	printMatrix(mat_c);
	matrixMul( mat_c, mat_b, mat_b, 0, 0, 0);
	printMatrix(mat_c);
}
