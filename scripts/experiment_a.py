#!/usr/bin/python

from lib.driver import SynthDriver
from lib.config import Config
import optparse
import os, sys
import Queue

config = None
source = None
kernel = None
testbench = None
threads = 1
verbose = True

## Parse input arguments and assign global variables
def parse_input():
    global source, config, testbench, kernel, threads, verbose
    parser = optparse.OptionParser()
    parser.set_defaults(verbose=True,threads=1)
    parser.add_option("-n", "--threads", dest='threads', type="int",    \
            help="Maximum number of threads")
    parser.add_option("-k", "--kernel", dest='kernel', type="string",   \
        help="Function Name of Top Level Kernel Function to be Synthesised")
    parser.add_option("-t", "--testbench", dest='testbench', type="string", \
        help="C Testbench to validate operation of Synthesised Kernel")
    parser.add_option("-s", "--source", dest='source', type="string",   \
        help="Kernel Source File. May Contain Multiple Kernels.")
    parser.add_option("-c", "--config", dest='config', type="string",   \
        help="Configuration file for Kernel to be Synthesised")
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose",   \
        help="Enable Verbose Output to STDOUT" )
    parser.add_option("-q", "--quiet", action="store_false", dest="verbose",    \
        help="Surpress Verbose Output. STDERR is still piped to STDOUT" )
    (options, args) = parser.parse_args()
    if hasattr(options, 'source'):
        source = options.source
    if hasattr(options, 'config'):
        config = options.config
    if hasattr(options, 'testbench'):
        testbench = options.testbench
    if hasattr(options, 'kernel'):
        kernel = options.kernel
    if hasattr(options, 'threads'):
        threads = options.threads
    if hasattr(options, 'verbose'):
        verbose = options.verbose
    if config == None or source == None or kernel == None or testbench == None:
        parser.print_help()

def create_configs( filename ):
    cwd = os.getcwd()
    variable_param  = 'workgroup_unrollfactor'
    variable_args   = ['0','1','2']
    variable_values = ['0','1','2','4','8','16']
    base_config = Config( filename )
    if not base_config.parameter_exists( variable_param ):
        raise Exception('Variable does not exist in config')
    config_list = []
    for value in variable_values:
        for arg in variable_args:
            filename = variable_param + '_'  + str(arg) + '_' + str(value) + '.cfg'
            path = cwd + '/tmp_cfg/' + filename
            print "Generating config file: " + path
            new_config = base_config
            new_config.set_value( variable_param, arg, value )
            new_config.write( path )
            config_list.append( path )
    return config_list

def main():
    global config
    wd = os.getcwd()
    parse_input()
    try:
        ## Create config files
        config_list = create_configs( config )
        ## Queue up threads
        thread_queue = Queue.Queue()
        for config in config_list:
            thread = SynthDriver( source=source, testbench=testbench, \
                    top=kernel, config=config, verbose=verbose )
            thread_queue.put( thread )

        thread_list = []
        ## Maintain at least 2 threads concurrently
        while not thread_queue.empty():
            ## Add new threads till max
            if len( thread_list ) < 2:
                new_thread = thread_queue.get()
                new_thread.start()
                thread_list.append( new_thread )
            for thread in thread_list:
                if not thread.isAlive():
                    ## Get thread results here
                    thread.handled = True
            ## Remove inactive threads
            thread_list = [t for t in thread_list if not t.handled]
            
    except Exception, e:
        print "Thread threw exception:"
        print e
        e_type, e_obj, e_tb = sys.exc_info()
        print e_type 
        print "Line: " + str(e_tb.tb_lineno)
        pass

if __name__ == "__main__":
    main()
