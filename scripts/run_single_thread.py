#!/usr/bin/python

from lib.driver import SynthDriver
import optparse
import os

config = None
source = None
kernel = None
testbench = None
threads = 1
verbose = True

## Parse input arguments and assign global variables
def parse_input():
    global source, config, testbench, kernel, threads, verbose
    parser = optparse.OptionParser()
    parser.set_defaults(verbose=True,threads=1)
    parser.add_option("-n", "--threads", dest='threads', type="int",    \
            help="Maximum number of threads")
    parser.add_option("-k", "--kernel", dest='kernel', type="string",   \
        help="Function Name of Top Level Kernel Function to be Synthesised")
    parser.add_option("-t", "--testbench", dest='testbench', type="string", \
        help="C Testbench to validate operation of Synthesised Kernel")
    parser.add_option("-s", "--source", dest='source', type="string",   \
        help="Kernel Source File. May Contain Multiple Kernels.")
    parser.add_option("-c", "--config", dest='config', type="string",   \
        help="Configuration file for Kernel to be Synthesised")
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose",   \
        help="Enable Verbose Output to STDOUT" )
    parser.add_option("-q", "--quiet", action="store_false", dest="verbose",    \
        help="Surpress Verbose Output. STDERR is still piped to STDOUT" )
    (options, args) = parser.parse_args()
    if hasattr(options, 'source'):
        source = options.source
    if hasattr(options, 'config'):
        config = options.config
    if hasattr(options, 'testbench'):
        testbench = options.testbench
    if hasattr(options, 'kernel'):
        kernel = options.kernel
    if hasattr(options, 'threads'):
        threads = options.threads
    if hasattr(options, 'verbose'):
        verbose = options.verbose
    if config == None or source == None or kernel == None or testbench == None:
        parser.print_help()

def main():
    wd = os.getcwd()
    parse_input()
    try:
        thread_1 = SynthDriver(source=source, testbench=testbench, \
                top=kernel, config=config, verbose=verbose)
        thread_2 = SynthDriver(source=source, testbench=testbench, \
                top=kernel, config=config, verbose=verbose)
        thread_1.start()
        thread_2.start()
    except Exception, e:
        print "Thread threw exception"
        print e

if __name__ == "__main__":
    main()
