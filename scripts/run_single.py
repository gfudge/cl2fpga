#!/usr/bin/python

import optparse
import shutil
import subprocess
import os, sys, string, random
from lib.rpt_parser import *
from lib.tcl import *

def name_generator(length=12):
    chars=string.ascii_lowercase + string.digits 
    return ''.join( random.choice(chars) for _ in range(length) )
    

## Create directory with randomised name in tmp folder
def create_dir():
    directory = "/tmp/single/" + name_generator()
    if not os.path.exists(directory):
        os.makedirs(directory)
    else:
        ## Try again
        create_dir()
    ## With a valid directory, return the path
    return directory    
    

def main():
    ## Store CWD
    old_cwd = os.getcwd()
    v = optparse.Values()
    parser = optparse.OptionParser()
    parser.set_defaults(verbose=False)
    parser.add_option("-k", "--kernel", dest='kernel', type="string",   \
            help="Function Name of Top Level Kernel Function to be Synthesised")
    parser.add_option("-t", "--testbench", dest='testbench', type="string", \
            help="C Testbench to validate operation of Synthesised Kernel")
    parser.add_option("-s", "--source", dest='source', type="string",   \
            help="Kernel Source File. May Contain Multiple Kernels.")
    parser.add_option("-c", "--config", dest='config', type="string",   \
            help="Configuration file for Kernel to be Synthesised")
    parser.add_option("-v", "--verbose", action="store_true", dest="verbose",   \
            help="Enable Verbose Output to STDOUT" )
    parser.add_option("-q", "--quiet", action="store_false", dest="verbose",    \
            help="Surpress Verbose Output. STDERR is still piped to STDOUT" )
    (options, args) = parser.parse_args()
    print options.verbose

    DEVNULL = open( os.devnull, 'w' )

    if hasattr(options, 'testbench') and hasattr(options, 'source') and \
        hasattr(options, 'config') and hasattr(options, 'kernel'):
        working_dir = create_dir()
        print "[+] Created Working Directory: " + working_dir

        # Copy over required files
        shutil.copyfile(options.testbench, working_dir + "/testbench.c")
        shutil.copyfile(options.source, working_dir + "/kernel.cl")
        shutil.copyfile(options.config, working_dir + "/kernel.cfg")
        print "[+] Copied files: " + options.testbench + ", " + \
                options.source + ", " + options.config

        print "[*] Changing working directory to: " + working_dir
        os.chdir( working_dir )
        rwkernel_args = ['RewriteKernel', 'kernel.cl', 'kernel.cfg']
        print "[*] Attempting Source-to-source translation on Kernel: " + options.kernel
        if options.verbose:
            ret_code = subprocess.call( rwkernel_args )
        else:
            ret_code = subprocess.call( rwkernel_args, stdout=DEVNULL, stderr=subprocess.STDOUT )
        if( ret_code != 0 ):
            print "[-] Rewriting Failed, exiting..."
            sys.exit(0)
        else:
            print "[+] Translation appeared successful..."
            print "[*] Proceeding with Synthesis"

        print "[+] Rewritten Source"
        project = "project-" + name_generator(length=6)
        solution = "solution-" + name_generator(length=6)
        top_func = options.kernel
        script = working_dir + "/run_hls.tcl"
        print "[*] Project Name: " + project
        print "[*] Solution Name: " + solution

        mytcl = GenerateTCL(project, top_func, 
                working_dir + "/kernel-hls.c", 
                working_dir + "/testbench.c", 
                'xc7z020clg484-1', '10', 
                solution )
        mytcl.create_script( script )
        print "[+] Generated TCL script"

        print "[*] Attempting to Synthesise and Test Kernel..."
        hls_args = ['vivado_hls', '-f', script]
        
        if options.verbose:
            ret_code = subprocess.call( hls_args )
        else:
            ret_code = subprocess.call( hls_args, stdout=DEVNULL, stderr=subprocess.STDOUT )
        
        if( ret_code != 0 ):
            print "[-] HLS Failed, exiting..."
            sys.exit(1)
        else:
            print "[+] HLS Returned Success, exiting"
            sys.exit(0)
    else:
        parser.print_help()

    os.chdir( old_cwd )

if __name__ == "__main__":
    main()
