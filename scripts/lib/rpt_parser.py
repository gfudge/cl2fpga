#!/usr/bin/python

import math
import xml.etree.ElementTree as ET

class RPT_Parser():
    
    def __init__(self, filename):
        self.tree = ET.parse( filename )
        self.root = tree.getroot()
        self.parse_utilisation()
        self.parse_timing()
        self.parse_performance()

    def parse_utilisation(self):
        area_estimates  = self.root.find( 'AreaEstimates' )

        used_resources  = area_estimates.find( 'Resources' )
        used_bram       = used_resources.find( 'BRAM_18K' )
        used_dsp        = used_resources.find( 'DSP48E' )
        used_ff         = used_resources.find( 'FF' )
        used_lut        = used_resources.find( 'LUT' )

        total_resources = area_estimates.find( 'AvailableResources' )
        total_bram      = total_resources.find( 'BRAM_18K' )
        total_dsp       = total_resources.find( 'DSP48E' )
        total_ff        = total_resources.find( 'FF' )
        total_lut       = total_resources.find( 'LUT' )

        self.bram_utilisation   = float(used_bram)/float(total_bram)
        self.dsp_utilisation    = float(used_dsp)/float(total_dsp)
        self.ff_utilisation     = float(used_ff)/float(total_ff)
        self.lut_utilisation    = float(used_lut)/float(total_lut)

    def parse_timing(self):
        timing_requested    = self.root.find( 'UserAssignments' )
        timing_estimates    = self.root.find( 'EstimatedClockPeriod' )
        self.target_time    = float( self.timing_requested.find( 'TargetClockPeriod' ) )
        self.uncertainty    = float( self.timing_requested.find( 'ClockUncertainty' ) )
        self.estimate_time  = float(timing_estimates)

    def parse_performance(self):
        overall_latency         = self.root.find( 'SummaryOfOverallLatency' )
        self.best_latency       = float(self.overall_latency.find( 'Best-caseLatency' ))
        self.average_latency    = float(self.overall_latency.find( 'Average-caseLatency' ))
        self.worst_latency      = float(self.overall_latency.find( 'Worst-caseLatency' ))

    # Calculate a normalised utilisation metric
    # Difference squared
    # M = (min - max)^2
    def calc_utilisation_metric(self):
        minimum = min( self.bram_utilisation, 
                self.dsp_utilisation, 
                self.ff_utilisation, 
                self.lut_utilisation )
        maximum = max( self.bram_utilisation,
                self.dsp_utilisation,
                self.ff_utilisation,
                self.lut_utilisation)
        metric = ( minimum - maximum )**2
        return metric

    # Calculate timing metric (penalty)
    # M = exp^(e-t/t)^2
    def calc_timing_metric(self):
        e = self.estimate_time
        t = self.target_time
        x = ( (e-t)/t )**2
        metric = exp( x )
        return metric

    # TODO Figure out some specified metric
    def calc_performance_metric(self):
        metric = self.average_latency
        return metric
