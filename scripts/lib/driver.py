#!/usr/bin/python

import subprocess, os, sys, random, string
import shutil
import threading
import time
from tcl import GenerateTCL

def str_gen(length=12):
    chars=string.ascii_lowercase + string.digits
    return ''.join( random.choice(chars) for _ in range(length) )

class SynthDriver(threading.Thread):

    def __init__(self, verbose=True, 
            config="config.cfg", source="kernel.cl",
            top="kernel", testbench="testbench.c"):
        
        ## Call Thread Constructor
        self.threadname = "synthdriver_" + str_gen(length=16)
        threading.Thread.__init__( self, name=self.threadname )
        
        ## Set verbosity before used
        self.verbose = verbose
        self._message("[+] Thread Created")
        self._message("[*] ThreadID = " + self.threadname)

        ## Save CWD
        self.original_working_dir = os.getcwd()

        ## Check if files exist
        if not os.path.isfile( source ):
            self._message("[-] ERROR: Source file does not exist")
            raise Exception('Source file does not exist')
        elif not os.path.isfile( config ):
            self._message("[-] ERROR: Config file does not exist")
            raise Exception('Config file does not exist')
        elif not os.path.isfile( testbench ):
            self._message("[-] ERROR: Testbench does not exist")
            raise Exception('Testbench file does not exist')
        else:
            self.source = source
            self.config = config
            self.top    = top
            self.testbench = testbench
            self._message("[+] Source Files Exist")
            self._create_directory()
            self._copy_files()

    def _create_directory(self):
        ## Create the driver working path
        try_path = "/tmp/synth/driver/" + str_gen(length=10)
        if os.path.exists( try_path ):
            ## If path exists, try again
            self.create_directory()
        else:
            ## Create a new directory
            os.makedirs( try_path )
            self.working_directory = try_path
            self._message("[+] Created Directory: " + try_path )
            
    def _copy_files(self):
        ## Copy files into the working directory
        self.config_path = self.working_directory + "/config.cfg"
        self.source_path = self.working_directory + "/kernel.cl"
        self.testbench_path = self.working_directory + "/tb_kernel.c"
        shutil.copyfile( self.config, self.config_path )
        shutil.copyfile( self.source, self.source_path )
        shutil.copyfile( self.testbench, self.testbench_path )
        if not os.path.isfile( self.config_path ):
            self._message("[-] ERROR: File did not copy")
            raise Exception('Config file failed to copy')
        elif not os.path.isfile( self.source_path ):
            self._message("[-] ERROR: File did not copy")
            raise Exception('Source file failed to copy')
        elif not os.path.isfile( self.testbench_path ):
            self._message("[-] ERROR: File did not copy")
            raise Exception('Testbench file failed to copy')
        else:
            self._message("[+] Files Copied")
            return True

    def _change_dir(self):
        os.chdir( self.working_directory )
        if( self.working_directory != os.getcwd() ):
            self._message("[-] ERROR: Could not change to working_directory")
            raise Exception('Could not change to correct working_directory')
        else:
            self._message("[+] Working Directory changed to: " + self.working_directory)
            return True

    def _translate_source(self):
        ## Check source file exists
        if not os.path.isfile( self.source_path ):
            self._message("[-] Translation failed: Source file does not exist")
            return False
        else:
            self._message("[*] Translating source: " + self.source_path )
            self.hls_source_path = self.working_directory + "/kernel-hls.c"
            args = ['RewriteKernel', self.source_path, self.config_path ]
            if self.verbose:
                p = subprocess.Popen( args, cwd=self.working_directory )
            else:
                DEVNULL = open( os.devnull, 'w' )
                p = subprocess.Popen( args, cwd=self.working_directory, \
                        stdout=DEVNULL, stderr=subprocess.STDOUT )
            ## Wait for process to exit
            ret = p.wait()    
            if( ret != 0 ):
                self._message("[-] ERROR: RewriteKernel returned not zero")
                raise Exception('RewriteKernel returned an error')
            elif not os.path.isfile( self.hls_source_path ):
                self._message("[-] ERROR: Rewritten file not found")
                raise Exception('Rewritten source file not found')
            else:
                self._message("[+] Source Rewritten")
                return True


    def _generate_tcl(self):
        self.project_name = "prj-" + str_gen(length=8)
        self.solution_name = "sol-" + str_gen(length=8)
        script = GenerateTCL( self.project_name,
                                self.top,
                                self.hls_source_path,
                                self.testbench_path,
                                'xc7z020clg484-1',
                                '10',
                                self.solution_name )
        self.script_path = self.working_directory + "/run_hls.tcl"
        script.create_script( self.script_path )
        if not os.path.isfile( self.script_path ):
            self._message("[-] ERROR: Generated Script not found")
            raise Exception('Generated script not found')
        else: 
            self._message("[+] Generated TCL Script")
            return True

    def _run_hls(self):
        args = [ 'vivado_hls', '-f', self.script_path ]
        self._message("[*] Launching Vivado HLS")
        if self.verbose:
            p = subprocess.Popen( args, cwd=self.working_directory )
        else:
            DEVNULL = open( os.devnull, 'w' )
            p = subprocess.Popen( args, cwd=self.working_directory,    \
                    stdout=DEVNULL, stderr=subprocess.STDOUT )
        ## Wait for process to finish
        ret = p.wait()
        if(ret != 0):
            self._message("[-] HLS Program returned error")
            raise Exception('HLS returned an error')
        else:
            self._message("[+] HLS Synthesis Succeeded")
            return True

    def _message(self, message):
        if self.verbose:
            print self.threadname + ": " + message
            sys.stdout.flush()

    def _cleanup(self):
        # Restore original working directory
        remove_dir = self.working_directory
        self.working_directory = self.original_working_dir
        self._change_dir()
        print remove_dir
        # Check working directory exists
        if os.path.isfile( remove_dir ):
            # Remove working directory
            self._message("[*] Removing Directory: " + remove_dir)
            #shutil.rmtree( self.working_directory )
            if not os.path.isfile( remove_dir ):
                self._message("[+] Working Directory successfully removed")
                return True
            else:
                self._message("[-] ERROR: Unable to remove working directory")
                raise Exception('Failed to remove working directory')
        else:
            self._message("[-] ERROR: Could not remove working directory")
            raise Exception('Working directory does not exist, cannot remove')

    def run(self):
        #self._change_dir()
        self._message("[*] Translating Source...")
        self._translate_source()
        self._generate_tcl()
        self._run_hls()
        #self._cleanup()
        self._message("[*] Synthesis Success")
        self._message("[*] Killing Thread")

