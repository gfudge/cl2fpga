#!/usr/bin/python

HEADER = "### AUTOMATICALLY GENERATED SCRIPT DO NOT MODIFY ###\n"

class GenerateTCL:

    def __init__(self, 
            project,
            top,
            source,
            testbench,
            device, 
            period,
            solution,
            csim=True,
            cosim=True,
            export=False):
        self.project    = project
        self.top        = top
        self.source     = source
        self.testbench  = testbench
        self.solution   = solution 
        self.device     = device
        self.period     = period
        self.csim       = csim
        self.cosim      = cosim
        self.export     = export

    def create_script_string(self):
        script = ""
        script += HEADER
        script += "open_project -reset " + self.project + "\n"
        script += "set_top " + self.top + "\n"
        script += "add_files " + self.source + "\n"
        script += "add_files -tb " + self.testbench + "\n"
        script += "open_solution " + self.solution + "\n"
        script += "set_part {" + self.device + "}\n"
        script += "create_clock -period " + self.period + " -name default\n"
        if self.csim:
            script += "csim_design\n"
        script += "csynth_design\n"
        if self.cosim:
            script += "cosim_design -rtl verilog -O\n"
        if self.export:
            script += "export_design -format ip_catalog\n"
        script += "exit\n"
        return script

    def create_script(self, filename):
        f = open(filename, 'w')
        f.write( self.create_script_string() )
        f.close()
        return
