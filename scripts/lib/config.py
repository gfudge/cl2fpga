#!/usr/bin/python
import sys
import pprint

class Config:
    def __init__(self, filename=None):
        self.data = {}
        if( filename != None ):
            self.read(filename)

    def set_value(self, param, arg, value):
        if param in self.data:
            if arg in self.data.get( param ):
                self.data[ param ][ arg ] = value
                return True
            else:
                self.data[ param ][ arg ] = value
                return True
        else:
            self.data[ param ] = dict({arg : value})
            #self.data[ param ][ arg ] = value
            return True

    def get_value(self, param, arg):
        return self.data[ param ][ arg ]
    
    def get_parameter_list(self):
        returnlist = []
        for key in self.data:
            returnlist.append( key )
        return returnlist

    def get_arg_list(self, param):
        returnlist = []
        for arg in self.data.get( param ):
            returnlist.append( arg )
        return returnlist

    def parameter_exists(self, param):
        if param in self.data:
            return True
        else:
            return False

    def read(self, filename):
        with open(filename, 'r') as f:
            for line in f:
                (param, arg, value) = self._parse_line( line )
                self.set_value( param, arg, value )

    def write(self, filename):
        with open(filename, 'w') as f:
            for param in self.data:
                for arg in self.data.get( param ):
                    value = self.get_value( param, arg )
                    f.write( self._create_line( param, arg, value ) )

    def dump(self):
        pprint.pprint( self.data )

    def _parse_line(self, line):
        strings = str(line).split('=')
        tmp = strings[0]
        value = strings[1].rstrip()
        try:
            start   = tmp.index( '(' ) + len( ')' )
            end     = tmp.index( ')', start )
            arg     = tmp[start:end]
            param   = tmp[0:start-1]
        except ValueError:
            arg = ""
        return (param, arg, value)
 
    def _create_line(self, param, arg, value):
        newline = str( param ) + '(' + arg + ')=' + value + '\n'
        return newline

if __name__ == "__main__":
    print "Reading: " + str( sys.argv[1] )
    cfg = Config( sys.argv[1] )
    cfg.dump()
