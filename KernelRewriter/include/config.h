#pragma once

#include <string>
#include <map>
#include <fstream>
#include <memory>
#include <iostream>

// Configuration class forward declaration
class Configuration;

// Global Configuration to be used by rewriter
extern std::shared_ptr<Configuration> Config;

// Type to store parsed configuration line strings
typedef struct {
    std::string type;
    std::string argument;
    std::string value;
} ConfigLine;

typedef enum {
    read_only,
    write_only,
    read_write
} InterfaceAccess;

//extern static std::map<std::string,InterfaceAccess> StringToAccessTypeMap;

class Configuration {
    public:
        Configuration( std::string filename );
        ~Configuration();
        
        // Workgroup configuration option querying methods
        size_t  getWorkgroupSize( size_t dim );
        size_t  getWorkgroupUnrollFactor( size_t dim);
        bool    getWorkgroupIsPipelined( size_t dim);

        // Interface configuration option querying methods
        bool        getInterfaceIsVolatile( std::string interface );
        size_t      getInterfaceArrayPartitionFactor( std::string interface );
        std::string getInterfacePortName( std::string interface );
        std::string getInterfaceBundleName( std::string interface );
        InterfaceAccess getInterfaceAccessType( std::string interface );
        size_t      getInterfaceDepth( std::string interface );

    private:
        ConfigLine                          tokenizeConfigLine( const std::string line );
        std::map<size_t,size_t>             WorkgroupSize;
        std::map<size_t,size_t>             WorkgroupUnrollFactor;
        std::map<size_t,bool>               WorkgroupIsPipelined;
        std::map<std::string,bool>          InterfaceIsVolatile;
        std::map<std::string,size_t>        InterfaceArrayPartitionFactor;
        std::map<std::string,std::string>   InterfacePortName;
        std::map<std::string,std::string>   InterfaceBundleName;
        std::map<std::string,InterfaceAccess>   InterfaceAccessType;
        std::map<std::string,size_t>        InterfaceDepth;
};
