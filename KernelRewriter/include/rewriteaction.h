#pragma once

#include <memory>

#include "clang/Frontend/FrontendAction.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/AST/ASTConsumer.h"

#include "rewritekernel.h"

class KernelRewriteAction : public clang::ASTFrontendAction {
public:
    virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(
            clang::CompilerInstance &CI, llvm::StringRef KernelFile)
    {
        std::string filename = KernelFile.str();
        size_t dotpos = filename.rfind('.');
        filename = filename.substr(0,dotpos) + "-hls" ;
        llvm::raw_ostream *KernelOutFile 
            = CI.createDefaultOutputFile(false, filename, "c");
        if( KernelOutFile ) {    
            return std::unique_ptr<clang::ASTConsumer>( new RewriteKernel(
                        &CI, KernelOutFile ) );
        }
        else {
            return nullptr;
        }

    }
};
