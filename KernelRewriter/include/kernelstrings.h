#pragma once

#define INCLUDE_DEF \
    "//#define <string.h>\n"

#define GLOBAL_ATTR_DEF \
    "#define __global   __attribute__((address_space(1)))\n"

#define LOCAL_ATTR_DEF \
    "#define __local    __attribute__((address_space(3)))\n"

#define CONSTANT_ATTR_DEF \
    "#define __constant __attribute__((address_space(2)))\n"

#define PRIVATE_ATTR_DEF \
    "#define __private  __attribute__((address_space(4)))\n"

#define WG_LOOP_BOUNDS \
    "\n"    \
    "#define WG_SIZE_Z  1   \n" \
    "#define WG_SIZE_Y  1   \n" \
    "#define WG_SIZE_X  64  \n" \
    "#define WG_SIZE_ALL    WG_SIZE_Z * WG_SIZE_Y * WG_SIZE_X \n" \
    "\n"

#define ARG_OFFSET_TYPE \
    "unsigned int"

#define ARG_OFFSET_X \
    "offset_x"

#define ARG_OFFSET_Y \
    "offset_y"

#define ARG_OFFSET_Z \
    "offset_z"

#define MASTER_IF_ARG_START  \
    "#pragma HLS INTERFACE m_axi port="

#define MASTER_IF_ARG_END    \
    " offset=slave bundle="

#define SLAVE_IF_ARG_START \
    "#pragma HLS INTERFACE s_axilite port="

#define SLAVE_IF_ARG_END \
    " bundle=control    \n"

#define SLAVE_IF_RETURN \
    "#pragma HLS INTERFACE s_axilite port=return bundle=control \n"

#define HLS_PIPELINE_OPT \
    "#pragma HLS pipeline\n"

#define MEMCPY_START    \
    "memcpy("

#define MEMCPY_END      \
    ");\n"

#define WG_FOR_LOOP_BEGIN \
    "\n" \
    "for( unsigned int WorkGroup_z=0; WorkGroup_z<WG_SIZE_Z; WorkGroup_z++ ) { \n" \
    "for( unsigned int WorkGroup_y=0; WorkGroup_y<WG_SIZE_Y; WorkGroup_y++ ) { \n" \
    "for( unsigned int WorkGroup_x=0; WorkGroup_x<WG_SIZE_X; WorkGroup_x++ ) { \n" \
    "\n\t"

#define WG_FOR_LOOP_Z \
    "\n" \
    "for( unsigned int WorkGroup_z=0; WorkGroup_z<WG_SIZE_Z; WorkGroup_z++ ) { \n" \
    "\n"

#define WG_FOR_LOOP_Y \
    "\n" \
    "for( unsigned int WorkGroup_y=0; WorkGroup_y<WG_SIZE_Y; WorkGroup_y++ ) { \n" \
    "\n"

#define WG_FOR_LOOP_X \
    "\n" \
    "for( unsigned int WorkGroup_x=0; WorkGroup_x<WG_SIZE_X; WorkGroup_x++ ) { \n" \
    "\n" 

#define WG_FOR_LOOP_END \
    "\n"    \
    "}\n"   \
    "}\n"   \
    "}\n"   \
    "\n"

#define PRAGMA_LOOP_PIPELINE \
    "#pragma HLS PIPELINE\n"

#define PRAGMA_UNROLL_FACTOR \
    "#pragma HLS UNROLL factor="

