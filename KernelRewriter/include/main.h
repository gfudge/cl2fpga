#pragma once

#include "rewriteaction.h"
#include "config.h"

#include <iostream>
#include <sstream>
#include <fstream>
#include <memory>

#include "clang/Tooling/Tooling.h"

// Reference the configuration defined in "config.h"
extern std::shared_ptr<Configuration> Config;
