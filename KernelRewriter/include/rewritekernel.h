#pragma once 

#include <memory>
#include <vector>
#include <map>
#include <list>
#include <string>
#include <sstream>
#include <iostream>

#include "clang/AST/AST.h"
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/Decl.h"

#include "clang/Basic/Diagnostic.h"
#include "clang/Basic/FileManager.h"
#include "clang/Basic/SourceManager.h"

#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendPluginRegistry.h"

#include "clang/Lex/Preprocessor.h"
#include "clang/Lex/PPCallbacks.h"

#include "clang/Rewrite/Core/Rewriter.h"
#include "clang/Rewrite/Frontend/Rewriters.h"

#include "llvm/Support/Path.h"
#include "llvm/Support/raw_ostream.h"
#include "llvm/Support/Regex.h"

#include "kernelstrings.h"
#include "config.h"

extern std::shared_ptr<Configuration> Config;

typedef std::map<clang::FileID, llvm::raw_ostream *> FileMap;

// Map: Parameter Name, Parameter Type
typedef std::map<std::string, std::string> ParamMap;

typedef struct { size_t x; size_t y; size_t z; } NDRangeSize;

class RewriteKernel : public clang::ASTConsumer {
    
public:
    RewriteKernel( clang::CompilerInstance *comp,
            llvm::raw_ostream *kernel) : CI(comp), KernelFile(kernel) {}

    virtual ~RewriteKernel() {}

    virtual void Initialize( clang::ASTContext &Context );
    virtual bool HandleTopLevelDecl( clang::DeclGroupRef DG );
    virtual void HandleTranslationUnit( clang::ASTContext &Context );

private:
    // Compiler References
    clang::CompilerInstance *CI;
    clang::SourceManager    *SM;
    clang::LangOptions      *LO;
    clang::Preprocessor     *PP;

    // Kernel Rewriter
    clang::Rewriter KernelRewrite;

    // Output File
    clang::FileID KernelFileID;
    llvm::raw_ostream *KernelFile;
    FileMap KernelFileMap;
 
    // Collected data about AST
    // Global NDRange size
    NDRangeSize GlobalSize;
    // Workgroup NDRange size
    NDRangeSize LocalSize;

    // Map of ptr arguments to their types
    ParamMap PtrParameterMap;
    ParamMap ConstParameterMap;
    ParamMap OffsetParameterMap;

    // Kernel Rewriters
    bool RewriteKernelFunction( clang::FunctionDecl *fd );
    bool RewriteKernelDeclaration( clang::FunctionDecl *fd );
    bool RewriteKernelAttr( clang::Attr *attr, std::string replace );
    bool RewriteKernelParam( clang::ParmVarDecl *pvd );
    bool RewriteKernelStmt( clang::Stmt *s );
    bool RewriteKernelExpr( clang::Expr *e );
    bool RewriteKernelForLoop( clang::ForStmt *fs );

    // Generate strings to be injected into source
    std::string createAttributeString();
    std::string createBRAMString();
    std::string createKernelIncludeString();
    std::string createWGSizeDefString();
    std::string createInterfacePragmaString();
    std::string createMemcpyReadString();
    std::string createMemcpyWriteString();
    std::string createWorkgroupForLoopStartString();
    std::string createWorkgroupForLoopEndString();

    // Get source string between two source locations
    std::string getKernelSourceBetween( clang::SourceLocation a, clang::SourceLocation b);

    // Kernel Strings to insert into file preamble
    std::string KernelAttrs;
    std::string KernelIncludes;
    std::string KernelDefs;
    std::string KernelDecls;
    std::string KernelGlobalVars;

};
