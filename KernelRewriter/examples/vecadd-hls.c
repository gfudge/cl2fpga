#define WG_SIZE_Z 1
#define WG_SIZE_Y 1
#define WG_SIZE_X 64
#define BRAM_SIZE 64


#define __global   __attribute__((address_space(1)))
#define __local    __attribute__((address_space(3)))
#define __constant __attribute__((address_space(2)))
#define __private  __attribute__((address_space(4)))

 void vecadd( __global float *if_a,
                      __global float *if_b,
                      __global float *if_c,
                      const unsigned int n, size_t offset_x, size_t offset_y, size_t offset_z) 
{
    #pragma HLS INTERFACE m_axi port=if_a offset=slave bundle=gmem  
#pragma HLS INTERFACE m_axi port=if_b offset=slave bundle=gmem  
#pragma HLS INTERFACE m_axi port=if_c offset=slave bundle=gmem  
#pragma HLS INTERFACE s_axilite port=if_a bundle=control    
#pragma HLS INTERFACE s_axilite port=if_b bundle=control    
#pragma HLS INTERFACE s_axilite port=if_c bundle=control    
#pragma HLS INTERFACE s_axilite port=n bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_x bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_y bundle=control    
#pragma HLS INTERFACE s_axilite port=offset_z bundle=control    
#pragma HLS INTERFACE s_axilite port=return bundle=control 

__global float a[BRAM_SIZE];
__global float b[BRAM_SIZE];
__global float c[BRAM_SIZE];
memcpy(a, (__global float *) if_a, BRAM_SIZE * sizeof(__global float) );
memcpy(b, (__global float *) if_b, BRAM_SIZE * sizeof(__global float) );
memcpy(c, (__global float *) if_c, BRAM_SIZE * sizeof(__global float) );

for( size_t WorkGroup_z=0; WorkGroup_z<WG_SIZE_Z; WorkGroup_z++ ) { 
for( size_t WorkGroup_y=0; WorkGroup_y<WG_SIZE_Y; WorkGroup_y++ ) { 
for( size_t WorkGroup_x=0; WorkGroup_x<WG_SIZE_X; WorkGroup_x++ ) { 

int idx = WorkGroup_x + offset_x;

    if( idx < n ) {
        c[idx] = a[idx] + b[idx];
    }

}
}
}

memcpy( (__global float *) if_a, a, BRAM_SIZE * sizeof(__global float) );
memcpy( (__global float *) if_b, b, BRAM_SIZE * sizeof(__global float) );
memcpy( (__global float *) if_c, c, BRAM_SIZE * sizeof(__global float) );
}
