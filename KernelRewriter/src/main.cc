#include "main.h"

std::string ReadFile( const char *filename ) {
    std::ifstream infile( filename );
    std::stringstream buffer;
    if( infile ) {
        buffer << infile.rdbuf();
        infile.close();
    }
    else {
        std::cout << "Unable to read in file" << std::endl;
    }
    return buffer.str();
}

void PrintHelp() {
    std::cout << "Usage: ./KernelRewriter <kernel.cl> <config.cfg>" << std::endl;
}

int main( int argc, const char **argv ) {
    if( argc > 1 ) {
        std::string input_file = ReadFile( argv[1] );
        // If second argument defined, open it as cfg file
        if( argc > 2 ) {
            Config = std::make_shared<Configuration>( argv[2] );
        }
        clang::tooling::runToolOnCode( new KernelRewriteAction, input_file, argv[1] );
        return 0;
    }
    else {
        PrintHelp();
        return 1;
    }
}
