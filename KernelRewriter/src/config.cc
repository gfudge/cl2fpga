#include "config.h"

std::shared_ptr<Configuration> Config = nullptr;
std::map<std::string,InterfaceAccess> StringToAccessTypeMap = {
    {"R",   read_only},
    {"W",   write_only},
    {"RW",  read_write}
};

Configuration::Configuration( std::string filename ) {
    std::ifstream config_file( filename, std::ifstream::in );
    std::string line;
    if( config_file.is_open() ) {
        while( std::getline( config_file, line ) ) {
            // Process each line
            auto config = tokenizeConfigLine( line );
            // Accept calls
            if( "workgroup_size" == config.type ) {
                auto dim = std::stoul( config.argument );
                auto value = std::stoul( config.value );
                this->WorkgroupSize[dim] = value;
            }
            else if( "workgroup_unrollfactor" == config.type ) {
                auto dim = std::stoul( config.argument );
                auto value = std::stoul( config.value );
                this->WorkgroupUnrollFactor[dim] = value;
            }
            else if( "workgroup_pipelined" == config.type ) {
                auto dim = std::stoul( config.argument );
                auto value = (std::stoul( config.value ) != 0) ? true : false;
                this->WorkgroupIsPipelined[dim] = value;
            }
            else if( "interface_volatile" == config.type ) {
                auto interface = config.argument;
                auto value = (std::stoul( config.value ) != 0) ? true : false;
                this->InterfaceIsVolatile[interface] = value;
            }
            else if( "interface_arraypartitionfactor" == config.type ) {
                auto interface = config.argument;
                auto value = std::stoul( config.value );
                this->InterfaceArrayPartitionFactor[interface] = value;
            }
            else if( "interface_portname" == config.type ) {
                auto interface = config.argument;
                auto value = config.value;
                this->InterfacePortName[interface] = value;
            }
            else if( "interface_bundlename" == config.type ) {
                auto interface = config.argument;
                auto value = config.value;
                this->InterfaceBundleName[interface] = value;
            }
            else if( "interface_accesstype" == config.type ) {
                auto interface = config.argument;
                auto value = config.value;
                if( value == "R" || value == "RW" || value == "W" ) {
                    this->InterfaceAccessType[interface] = StringToAccessTypeMap[value];
                }
                else {
                    this->InterfaceAccessType[interface] = StringToAccessTypeMap["RW"];
                }
            }
            else if( "interface_depth" == config.type ) {
                auto interface = config.argument;
                auto value = std::stoul( config.value );
                this->InterfaceDepth[interface] = value;
            }
            else {
            }
        }
    }
}

Configuration::~Configuration() {}

// Tokenize configuration line into ConfigLine structure
ConfigLine Configuration::tokenizeConfigLine( const std::string line ) {
    ConfigLine result;
    auto equals = line.find_first_of('=');
    auto first_brac = line.find_first_of('(');
    auto last_brac  = line.find_first_of(')');
    if( (first_brac != std::string::npos) && 
            (last_brac != std::string::npos) && 
            (equals != std::string::npos) ) 
    {
        result.type     = line.substr( 0, first_brac );
        result.argument = line.substr( first_brac + 1, last_brac - first_brac - 1 );
        result.value    = line.substr( equals + 1 );
    }
    return result;
}

size_t Configuration::getWorkgroupSize( size_t dim ) {
    auto pair = this->WorkgroupSize.find( dim );
    if( pair != this->WorkgroupSize.end() ) {
        return pair->second;
    }
    else {
        return 0;
    }
}

size_t Configuration::getWorkgroupUnrollFactor( size_t dim ) {
    auto pair = this->WorkgroupUnrollFactor.find( dim );
    if( pair != this->WorkgroupUnrollFactor.end() ) {
        return pair->second;
    }
    else {
        return 0;
    }
}

bool Configuration::getWorkgroupIsPipelined( size_t dim ) {
    auto pair = this->WorkgroupIsPipelined.find( dim );
    if( pair != this->WorkgroupIsPipelined.end() ) {
        return pair->second;
    }
    else {
        return false;
    }
}

bool Configuration::getInterfaceIsVolatile( std::string interface ) {
    auto pair = this->InterfaceIsVolatile.find( interface );
    if( pair != this->InterfaceIsVolatile.end() ) {
        return pair->second;
    }
    else {
        return 0;
    }
}

size_t Configuration::getInterfaceArrayPartitionFactor( std::string interface ) {
    auto pair = this->InterfaceArrayPartitionFactor.find( interface );
    if( pair != this->InterfaceArrayPartitionFactor.end() ) {
        return pair->second;
    }
    else {
        return 0;
    }
}

std::string Configuration::getInterfacePortName( std::string interface ) {  
    auto pair = this->InterfacePortName.find( interface );
    if( pair != this->InterfacePortName.end() ) {
        return pair->second;
    }
    else {
        return "";
    }
}

std::string Configuration::getInterfaceBundleName( std::string interface ) {
    auto pair = this->InterfaceBundleName.find( interface );
    if( pair != this->InterfaceBundleName.end() ) {
        return pair->second;
    }
    else {
        return "";
    }
}

InterfaceAccess Configuration::getInterfaceAccessType( std::string interface ) {
    auto pair = this->InterfaceAccessType.find( interface );
    if( pair != this->InterfaceAccessType.end() ) {
        return pair->second;
    }
    else {
        return read_write;
    }
}

size_t Configuration::getInterfaceDepth( std::string interface ) {
    auto pair = this->InterfaceDepth.find( interface );
    if( pair != this->InterfaceDepth.end() ) {
        return pair->second;
    }
    else {
        return 0;
    }
}
