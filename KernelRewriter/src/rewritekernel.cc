#include "rewritekernel.h"

void RewriteKernel::Initialize( clang::ASTContext &Context ) {
    SM = &Context.getSourceManager();
    LO = &CI->getLangOpts();
    PP = &CI->getPreprocessor();

    this->KernelRewrite.setSourceMgr(*SM, *LO);
    this->KernelFileID = SM->getMainFileID();
 
    // TODO: Global/Local size based on user input
    // We don't want to assign global work size at (pre)compile time!
    //GlobalSize.x = 64, GlobalSize.y = 1, GlobalSize.z = 1;
    LocalSize.x = Config->getWorkgroupSize( 0 );
    LocalSize.y = Config->getWorkgroupSize( 1 );
    LocalSize.z = Config->getWorkgroupSize( 2 );

    // Set Interface Offset Map
    OffsetParameterMap[ ARG_OFFSET_X ] = ARG_OFFSET_TYPE;
    OffsetParameterMap[ ARG_OFFSET_Y ] = ARG_OFFSET_TYPE;
    OffsetParameterMap[ ARG_OFFSET_Z ] = ARG_OFFSET_TYPE;
}

// Called after AST has been traversed
void RewriteKernel::HandleTranslationUnit( clang::ASTContext &Context ) {
    KernelIncludes = createWGSizeDefString();
    KernelAttrs = createAttributeString();
    KernelDefs = createKernelIncludeString();

    // Write file preamble to head of file
    std::string KernelPreamble;
    KernelPreamble  += KernelIncludes;
    KernelPreamble  += KernelDefs;
    KernelPreamble  += KernelDecls;
    KernelPreamble  += KernelGlobalVars;
    //KernelPreamble += KernelAttrs;
    KernelRewrite.InsertTextBefore( SM->getLocForStartOfFile(KernelFileID), 
            KernelPreamble );

    // True when input file is rewritten at all
    if( const clang::RewriteBuffer *RewriteBuf = 
            KernelRewrite.getRewriteBufferFor( KernelFileID )) {
        // Push rewritten file buffer to file stream
        *KernelFile << std::string( RewriteBuf->begin(), RewriteBuf->end() );
        std::cout << "Written file to output" << std::endl;
    }
    else {
        std::cout << "No changed written to output" << std::endl;
    }
    // Flush buffer to output file
    KernelFile->flush();
}

bool RewriteKernel::HandleTopLevelDecl( clang::DeclGroupRef DG ) {
    // Get first decl
    clang::Decl *first = DG.isSingleDecl() ? DG.getSingleDecl() : DG.getDeclGroup()[0];

    clang::SourceLocation loc = first->getLocation();

    for( clang::DeclGroupRef::iterator i = DG.begin(), e = DG.end();
            i != e; ++i ) {
        if( clang::FunctionDecl *fd = llvm::dyn_cast<clang::FunctionDecl>(*i) ) {
            // If it is a kernel and not implicit declaration
            if( fd->hasAttr<clang::OpenCLKernelAttr>() && !fd->isImplicit() ) {
                // Rewrite the function
                RewriteKernelFunction( fd );
            }
        }
    }
    return true;
}

bool RewriteKernel::RewriteKernelFunction( clang::FunctionDecl *fd ) {
    // If the decl has a body
    if( clang::Stmt *body = fd->getBody() ) {
        
        // Rewrite Function Declaration
        RewriteKernelDeclaration( fd );

        // Rewrite Statement
        RewriteKernelStmt( body );
        
        if( clang::CompoundStmt *cs = llvm::dyn_cast<clang::CompoundStmt>( body ) ) {
            // Get location before first statement
            clang::SourceLocation csStart = SM->getExpansionLoc( 
                    cs->body_front()->getLocStart() );
            // Get location at end brace
            clang::SourceLocation csEnd = SM->getExpansionLoc( 
                    cs->getRBracLoc() );

            std::string beginStmt, endStmt;
            // Synth interface pragmas
            beginStmt += createInterfacePragmaString();
            // Synth BRAM declarations
            beginStmt += createBRAMString();
            // Synth Memcpy operations
            beginStmt += createMemcpyReadString();

            // Inject at beginning of compound statement, after first bracket 
            KernelRewrite.InsertTextBefore( csStart, beginStmt );

            // Write front of for loop before first statement
            KernelRewrite.InsertTextAfter( csStart, createWorkgroupForLoopStartString() );
            // Write back of for loop after last statement
            KernelRewrite.InsertTextAfter( csEnd, createWorkgroupForLoopEndString() );

            // Generate Memcpy write back code
            endStmt += createMemcpyWriteString();
            KernelRewrite.InsertTextAfter( csEnd, endStmt );
        }
        return true;
    }
    else {
        return false;
    }
}

// Rewrite the Function declaration to be compatible with HLS interface
bool RewriteKernel::RewriteKernelDeclaration( clang::FunctionDecl *fd ) {
    
    // Iterate over each parameter in declaration
    for( clang::FunctionDecl::param_iterator pi = fd->param_begin(),
            pe = fd->param_end(); pi != pe; ++pi ) {
        // Push the Parameter Map onto vector for generating BRAM/Memcpy stmts
        RewriteKernelParam( *pi );
    }
    // Rewrite the Function Declaration to allow AXI4 interface
    // Get __kernel attribute
    if( clang::OpenCLKernelAttr *attr = fd->getAttr<clang::OpenCLKernelAttr>() ) {
        // Replace Attr with empty string
        RewriteKernelAttr( attr, "" );
    }

    // Rewrite Function Declaration - add arguments and modify existing args
    std::string extra_args;
    extra_args += ", ";
    extra_args += ARG_OFFSET_TYPE;
    extra_args += " ";
    extra_args += ARG_OFFSET_X;
    extra_args += ", ";
    extra_args += ARG_OFFSET_TYPE;
    extra_args += " ";
    extra_args += ARG_OFFSET_Y;
    extra_args += ", ";
    extra_args += ARG_OFFSET_TYPE;
    extra_args += " ";
    extra_args += ARG_OFFSET_Z;

    // Get source location of last parameter
    const clang::ParmVarDecl *last = fd->parameters().back();
    clang::SourceLocation fd_end = last->getSourceRange().getEnd();
    KernelRewrite.InsertTextAfterToken( fd_end, extra_args );
    

    // Return false if no parameter information obtained
    return true;
}

// Replace an attribute with a string
bool RewriteKernel::RewriteKernelAttr( clang::Attr *attr, std::string replace ) {
    clang::SourceLocation attrLoc = SM->getExpansionLoc( attr->getLocation() );
    clang::SourceRange expandedRange( attrLoc, PP->getLocForEndOfToken(attrLoc) );
    KernelRewrite.ReplaceText( attrLoc, 
            KernelRewrite.getRangeSize(expandedRange), replace);
    return true;
}

// Rewrite the function parameters
bool RewriteKernel::RewriteKernelParam( clang::ParmVarDecl *pvd ) {    
    
    // Get TypeLoc info
    clang::TypeLoc tl = pvd->getTypeSourceInfo()->getTypeLoc();
    // Get underlying dereferenced type
    clang::QualType qt = pvd->getOriginalType().getNonReferenceType();
    // Check that type is not null type
    if( qt.isNull() ) {
        return false;
    }
    
    // Get Unqualified type
    const clang::Type *t = qt.getTypePtr();

    // TODO: Make pointer type volatile (if not already)
    // Handle pointer args
    if( t->isPointerType() ) {
        // Get the pointee type
        clang::QualType pointee = t->getPointeeType();

        std::string volatileToken;
        // Check if type is volatile qualified
        if( !pointee.isVolatileQualified() ) {
            // Add the volatile keyword (plus space) to volatile token
            volatileToken += "volatile ";
        }

        std::string token;
        std::string strippedParameter;
        // Initialise stringstream with type
        std::stringstream ParamLine( pointee.getAsString() );
        while( std::getline( ParamLine, token, ' ' ) ) {
            if( token != "__global" ) {
            // If not global, add to parameter
                strippedParameter += token;
            }
        }

        // Add to pointer parameter map
        PtrParameterMap[ pvd->getNameAsString() ] = strippedParameter;
        
        // Create new interface parameter name
        std::string  newParamName;
        newParamName += volatileToken;
        newParamName += strippedParameter;
        newParamName += " *";
        newParamName += "if_";
        newParamName += pvd->getNameAsString();
       
        // Get parameter source range
        clang::SourceRange sr = pvd->getSourceRange();
        // Replace parameter name source range
        KernelRewrite.ReplaceText( sr, newParamName );

    }
    // Handle non pointer args
    else {
        // Add to non-pointer parameter map
        // These will use a AXI4-Lite slave interface
        ConstParameterMap[ pvd->getNameAsString() ] = qt.getAsString();
    }

    return true;
}

bool RewriteKernel::RewriteKernelStmt( clang::Stmt *s ) {
    // Check visiting node is not a nullptr
    if( !s ) {
        return false;
    }
    // Resolve OpenCL calls pre-compile time
    if( clang::Expr *e = llvm::dyn_cast<clang::Expr>( s ) ) {
        // If we can cast Stmt as an Expr, rewrite the Expr
        RewriteKernelExpr( e );
    }
    // Force pipeline loops within work item?
    else if( clang::ForStmt *fs = llvm::dyn_cast<clang::ForStmt>( s ) ) {
        // TODO: Add pragma to internal for loops
        RewriteKernelForLoop( fs );
    }
    // Else traverse children
    else {
        for( clang::Stmt::child_iterator ci = s->child_begin(),
                ce = s->child_end(); ci != ce; ++ci ) {
            RewriteKernelStmt( *ci );
        }
    }
    return true;
}

bool RewriteKernel::RewriteKernelExpr( clang::Expr *e ) {
    if( !e ) {
        return false;
    }
    if( e->getSourceRange().isInvalid() ) {
        return false;
    }
    // Handle Call Expressions
    if ( clang::CallExpr *ce = llvm::dyn_cast<clang::CallExpr>(e) ) {
        std::string func_name = ce->getDirectCallee()->getNameAsString();
        clang::SourceRange exprRange(SM->getExpansionLoc(e->getLocStart() ),
                SM->getExpansionLoc(e->getLocEnd() ));
        
        // TODO: Handle Barrier CallExpr
        if( func_name == "barrier" ) {
            
        }
        // Handle Global ID thread resolution
        else if( func_name == "get_global_id" ) {
            // Determine which dimension from argument
            if( ce->getNumArgs() == 1 ) {
                clang::Expr *arg = ce->getArg(0);
                std::string arg_str 
                    = getKernelSourceBetween( arg->getExprLoc(), ce->getLocEnd() );
                if( arg_str.find("0") != std::string::npos ) {
                    // Replace CallExpr with x dimension iterator
                    std::string var = "WorkGroup_x + ";
                    var += ARG_OFFSET_X;
                    KernelRewrite.ReplaceText( exprRange, var );
                }
                else if( arg_str.find("1") != std::string::npos ) {
                    // Replace CallExpr with y dimension iterator
                    std::string var = "WorkGroup_y + ";
                    var += ARG_OFFSET_Y;
                    KernelRewrite.ReplaceText( exprRange, var );
                }
                else if( arg_str.find("2") != std::string::npos ) {
                    // Replace CallExpr with z dimension iterator
                    std::string var = "WorkGroup_z + ";
                    var += ARG_OFFSET_Z;
                    KernelRewrite.ReplaceText( exprRange, var );
                }
            }
        }
        // Handle Local ID Thread Resolution
        else if( func_name == "get_local_id" ) {
            // Determine which dimension from argument
            if( ce->getNumArgs() == 1 ) {
                clang::Expr *arg = ce->getArg(0);
                std::string arg_str
                    = getKernelSourceBetween( arg->getExprLoc(), ce->getLocEnd() );
                if( arg_str.find("0") != std::string::npos ) {
                    // Replace CallExpr with Local Thread ID dimension x
                    KernelRewrite.ReplaceText( exprRange, "WorkGroup_x" );
                }
                else if( arg_str.find("1") != std::string::npos ) {
                    // Replace CallExpr with Local Thread ID dimension y
                    KernelRewrite.ReplaceText( exprRange, "WorkGroup_y" );
                }
                else if( arg_str.find("2") != std::string::npos ) {
                    // Replace CallExpr with Local Thread ID dimension z
                    KernelRewrite.ReplaceText( exprRange, "WorkGroup_z" );
                }
            }
        }
        // Handle Global thread size resolution
        // TODO: This will not likely be supported as 
        // we do not know the true value of GlobalSize
        // at compile time.
        else if( func_name == "get_global_size" ) {
            if( ce->getNumArgs() ) {
                clang::Expr *arg = ce->getArg(0);
                std::string arg_str
                    = getKernelSourceBetween( arg->getExprLoc(), ce->getLocEnd() );
                if( arg_str.find("0") != std::string::npos ) {
                    KernelRewrite.ReplaceText( exprRange, 
                            std::to_string(this->GlobalSize.x) );
                }
                else if( arg_str.find("1") != std::string::npos ) {
                    KernelRewrite.ReplaceText( exprRange, 
                            std::to_string(this->GlobalSize.y) );
                }
                else if( arg_str.find("2") != std::string::npos ) {
                    KernelRewrite.ReplaceText( exprRange, 
                            std::to_string(this->GlobalSize.z) );
                }
            }
        }
        // Handle Local thread size resolution
        else if( func_name == "get_local_size" ) {
            if( ce->getNumArgs() ) {
                clang::Expr *arg = ce->getArg(0);
                std::string arg_str
                    = getKernelSourceBetween( arg->getExprLoc(), ce->getLocEnd() );
                if( arg_str.find("0") != std::string::npos ) {
                    // Replace CallExpr with workgroup size dimension x
                    KernelRewrite.ReplaceText( exprRange, 
                            std::to_string(this->LocalSize.x) );
                }
                else if( arg_str.find("1") != std::string::npos ) {
                    // Replace CallExpr with workgroup size dimension y
                    KernelRewrite.ReplaceText( exprRange,
                            std::to_string(this->LocalSize.y) );
                }
                else if( arg_str.find("2") != std::string::npos ) {
                    // Replace CallExpr with workgroup size dimension z
                    KernelRewrite.ReplaceText( exprRange,
                            std::to_string(this->LocalSize.z) );
                }
            }
        }
        else {
            // Print the call is not supported
            std::cout << "Unsupported Call: " << func_name << std::endl;
        }
    }
    return true;
}

/* Rewrite For Loop Statement to contain necessary pragmas */
bool RewriteKernel::RewriteKernelForLoop( clang::ForStmt *fs ) {
    // TODO: Make this do something
    return true;
}

std::string RewriteKernel::createAttributeString() {
    std::string AttrString;
    AttrString += "\n";
    AttrString += GLOBAL_ATTR_DEF;
    AttrString += LOCAL_ATTR_DEF;
    AttrString += CONSTANT_ATTR_DEF;
    AttrString += PRIVATE_ATTR_DEF;
    AttrString += "\n";
    return AttrString;
}

std::string RewriteKernel::createKernelIncludeString() {
    std::string includes = INCLUDE_DEF;
    includes += "\n";
    return includes;
}

// Generate Includes for fixed Local Workgroup Size
std::string RewriteKernel::createWGSizeDefString() {
    size_t bram_size = LocalSize.z * LocalSize.y * LocalSize.x;
    
    std::string IncludeString;
    IncludeString += "#define WG_SIZE_Z ";
    IncludeString += std::to_string(this->LocalSize.z);
    IncludeString += "\n";
    IncludeString += "#define WG_SIZE_Y ";
    IncludeString += std::to_string(this->LocalSize.y);
    IncludeString += "\n";
    IncludeString += "#define WG_SIZE_X ";
    IncludeString += std::to_string(this->LocalSize.x);
    IncludeString += "\n";
    IncludeString += "#define BRAM_SIZE ";
    IncludeString += std::to_string(bram_size);
    IncludeString += "\n\n";
    return IncludeString;
}

std::string RewriteKernel::createBRAMString() {
    std::string BRAMString;
    // Only create BRAMs for parameters that point to memory
    for( auto& Param : PtrParameterMap ) {
        std::string BRAM;
        BRAM += Param.second;
        BRAM += " ";
        BRAM += Param.first;
        BRAM += "[BRAM_SIZE];\n";
        BRAMString += BRAM;
    }
    return BRAMString;
}

std::string RewriteKernel::createInterfacePragmaString() {
    std::string InterfacePragma, MasterInterfacePragma, SlaveInterfacePragma;
    
    // Create AXI4 master and lite (slave) interfaces for pointer types
    for( auto& Param : PtrParameterMap ) {
        std::string m_pragma, s_pragma, param_arg, bundle;
        // If configuration exists for interface, use that, else use default
        if( Config->getInterfacePortName( Param.first ) != "" ) {
            param_arg = Config->getInterfacePortName( Param.first );
        }
        // Else, set default name
        else {
            param_arg += "if_" + Param.first;
        }
        // Generate Master Pragma for parameter
        m_pragma += MASTER_IF_ARG_START;
        m_pragma += param_arg;
        m_pragma += MASTER_IF_ARG_END;
        // Calculate required pipeline depth
        size_t depth = Config->getInterfaceDepth( Param.first );
        // If depth not set in config, set as workgroup size (x*y*z)
        // This is inefficient and will cause excess copying if not set
        if( 0 == depth ) {
            depth = Config->getWorkgroupSize( 0 ) * 
                Config->getWorkgroupSize( 1 ) * 
                Config->getWorkgroupSize( 2 );
        }

        // If bundle is defined for this
        if( Config->getInterfaceBundleName( Param.first ) != "" ) {
            bundle = Config->getInterfaceBundleName( Param.first );
        }
        // Else default bundle
        else {
            bundle = "gmem";
        }
        m_pragma += bundle;

        // Attach pipeline depth data
        m_pragma += " depth=";
        m_pragma += std::to_string( depth );
        m_pragma += "\n";
        
        // Generate Control slave pragma for parameter
        s_pragma += SLAVE_IF_ARG_START;
        s_pragma += param_arg;
        s_pragma += SLAVE_IF_ARG_END;

        // Add these pragmas to total list
        MasterInterfacePragma   += m_pragma;
        SlaveInterfacePragma    += s_pragma;
    }
    // Create AXI4-Lite interfaces only for non-pointer types
    for( auto& Param : ConstParameterMap ) {
        std::string s_pragma;
        s_pragma += SLAVE_IF_ARG_START;
        s_pragma += Param.first;
        s_pragma += SLAVE_IF_ARG_END;
        SlaveInterfacePragma += s_pragma;
    }
    // Create AXI4-Lite interface for offset parameters
    for( auto& Param : OffsetParameterMap ) {
        std::string s_pragma;
        s_pragma += SLAVE_IF_ARG_START;
        s_pragma += Param.first;
        s_pragma += SLAVE_IF_ARG_END;
        SlaveInterfacePragma += s_pragma;
    }

    InterfacePragma += MasterInterfacePragma;
    InterfacePragma += SlaveInterfacePragma;
    InterfacePragma += SLAVE_IF_RETURN;
    InterfacePragma += "\n";

    return InterfacePragma;
}

std::string RewriteKernel::createMemcpyReadString() {
    std::string MemcpyRead;
    for( auto& Param : PtrParameterMap ) {
        // Check if the Parameter is R/RW
        auto access_type = Config->getInterfaceAccessType( Param.first );
        if( read_only == access_type || read_write == access_type ) {
            std::string param_arg, memcpy_str;
            size_t depth = Config->getInterfaceDepth( Param.first );
            // If depth not defined in config, set to 'default'
            // WARNING: This is likely to cause an out of bounds memcpy!!!
            if( 0 == depth ) {
                depth = this->LocalSize.x * this->LocalSize.y * this->LocalSize.z;
            }
            param_arg += "if_" + Param.first;
            memcpy_str += MEMCPY_START;
            memcpy_str += Param.first;
            memcpy_str += ", (";
            memcpy_str += Param.second;
            memcpy_str += " *) ";
            memcpy_str += param_arg;
            memcpy_str += ", ";
            memcpy_str += std::to_string( depth );
            memcpy_str += " * sizeof(";
            memcpy_str += Param.second;
            memcpy_str += ") ";
            memcpy_str += MEMCPY_END;
            MemcpyRead += memcpy_str;
        }
    }
    return MemcpyRead;
}

std::string RewriteKernel::createMemcpyWriteString() {
    std::string MemcpyWrite;
    for( auto& Param: PtrParameterMap ) {
        auto access_type = Config->getInterfaceAccessType( Param.first );
        if( write_only == access_type || read_write == access_type ) {
            std::string param_arg, memcpy_str;
            size_t depth = Config->getInterfaceDepth( Param.first );
            if( 0 == depth ) {
                depth = this->LocalSize.x * this->LocalSize.y * this->LocalSize.z;
            }
            param_arg += "if_" + Param.first;
            memcpy_str += MEMCPY_START;
            memcpy_str += " (";
            memcpy_str += Param.second;
            memcpy_str += " *) ";
            memcpy_str += param_arg;
            memcpy_str += ", ";
            memcpy_str += Param.first;
            memcpy_str += ", ";
            memcpy_str += std::to_string( depth );
            memcpy_str += " * sizeof(";
            memcpy_str += Param.second;
            memcpy_str += ") ";
            memcpy_str += MEMCPY_END;
            MemcpyWrite += memcpy_str;
        }
    }
    return MemcpyWrite;
}

std::string RewriteKernel::createWorkgroupForLoopStartString() {
    std::string ForLoopStartStr;
    
    size_t zDimUnrollFactor = Config->getWorkgroupUnrollFactor( 2 );
    size_t yDimUnrollFactor = Config->getWorkgroupUnrollFactor( 1 );
    size_t xDimUnrollFactor = Config->getWorkgroupUnrollFactor( 0 );
    bool zDimIsPipelined = Config->getWorkgroupIsPipelined( 2 );
    bool yDimIsPipelined = Config->getWorkgroupIsPipelined( 1 );
    bool xDimIsPipelined = Config->getWorkgroupIsPipelined( 0 );

    ForLoopStartStr += WG_FOR_LOOP_Z;
    // z-dimension pragmas
    if( zDimIsPipelined ) {
        ForLoopStartStr += PRAGMA_LOOP_PIPELINE;
    }
    if( zDimUnrollFactor > 0 ) {
        ForLoopStartStr += PRAGMA_UNROLL_FACTOR;
        ForLoopStartStr += std::to_string( zDimUnrollFactor );
        ForLoopStartStr += "\n";
    }

    ForLoopStartStr += WG_FOR_LOOP_Y;
    // y-dimension pragmas
    if( yDimIsPipelined ) {
        ForLoopStartStr += PRAGMA_LOOP_PIPELINE;
    }
    if( yDimUnrollFactor ) {
        ForLoopStartStr += PRAGMA_UNROLL_FACTOR;
        ForLoopStartStr += std::to_string( yDimUnrollFactor );
        ForLoopStartStr += "\n";
    }

    ForLoopStartStr += WG_FOR_LOOP_X;
    // x-dimension pragmas
    if( xDimIsPipelined ) {
        ForLoopStartStr += PRAGMA_LOOP_PIPELINE;
    }
    if( xDimUnrollFactor ) {
        ForLoopStartStr += PRAGMA_UNROLL_FACTOR;
        ForLoopStartStr += std::to_string( xDimUnrollFactor );
        ForLoopStartStr += "\n";
    }

    return ForLoopStartStr;
}

std::string RewriteKernel::createWorkgroupForLoopEndString() {
    std::string ForLoopEndStr;
    ForLoopEndStr += WG_FOR_LOOP_END;
    return ForLoopEndStr;
}

std::string RewriteKernel::getKernelSourceBetween( 
        clang::SourceLocation a, clang::SourceLocation b ) {
    return std::string( SM->getCharacterData(a), 
                        SM->getCharacterData(b) - SM->getCharacterData(a) );
}
