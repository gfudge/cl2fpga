CXX     = g++
CXXFLAGS  = -fno-rtti -fno-exceptions -std=c++14 -fdata-sections -ffunction-sections

STRIPPER = strip
STRIPPERFLAGS = -s -R .comment -R .gnu.version

PROJECT_INC = -Iinclude
PROJECT_SRC_PATH = src
PROJECT_BIN_PATH = bin
BUILDDIR = build
ELFNAME = RewriteKernel
SOURCES = $(wildcard $(PROJECT_SRC_PATH)/*.cc)
OBJECTS = $(patsubst $(PROJECT_SRC_PATH)/%.cc,$(BUILDDIR)/%.o,$(SOURCES))

LLVM_SRC_PATH   = ~/clang-llvm/llvm
LLVM_BUILD_PATH = ~/clang-llvm/build

LLVM_BIN_PATH       = $(LLVM_BUILD_PATH)/bin
LLVM_LIBS           = core mc option support 
#LLVM_CONFIG_COMMAND = $(LLVM_BIN_PATH)/llvm-config --cxxflags     \
                                                    --ldflags       \
                                                    -libs $(LLVM_LIBS)

LLVM_CXX_FLAGS  = `$(LLVM_BIN_PATH)/llvm-config --cxxflags`
LLVM_LDFLAGS    = `$(LLVM_BIN_PATH)/llvm-config --ldflags --libs --system-libs`

CLANG_INCLUDES = -I$(LLVM_SRC_PATH)/tools/clang/include  \
                    -I$(LLVM_SRC_PATH)/include              \
                    -I$(LLVM_BUILD_PATH)/tools/clang/include
CXX_LIBS = -lstdc++fs

CLANG_LIBS =                        \
    -Wl,--start-group				\
	-lclangFrontendTool             \
    -lclangFrontend                 \
    -lclangDriver                   \
    -lclangSerialization            \
    -lclangCodeGen                  \
    -lclangParse                    \
    -lclangSema                     \
    -lclangStaticAnalyzerFrontend   \
    -lclangStaticAnalyzerCheckers   \
    -lclangStaticAnalyzerCore       \
    -lclangAnalysis                 \
    -lclangARCMigrate               \
    -lclangRewrite                  \
    -lclangEdit                     \
    -lclangAST                      \
    -lclangLex                      \
    -lclangBasic                    \
    -lclangToolingCore              \
    -lclangTooling                  \
	-Wl,--end-group

all: dir $(PROJECT_BIN_PATH)/$(ELFNAME) strip

nostrip: dir $(PROJECT_BIN_PATH)/$(ELFNAME)

strip:
	$(STRIPPER) $(STRIPPERFLAGS) $(PROJECT_BIN_PATH)/$(ELFNAME)

dir: 
	mkdir -p $(BUILDDIR)

install:
	cp $(PROJECT_BIN_PATH)/$(ELFNAME) /usr/local/bin

$(PROJECT_BIN_PATH)/$(ELFNAME): $(OBJECTS)
	@echo -e 'Linking objects: ' $(OBJECTS)
	$(CXX) $^ $(CXX_LIBS) $(CLANG_LIBS) $(LLVM_LDFLAGS) -o $@ -Wl,--gc-sections
	@echo -e 'Stripping binary of unused functions'
	#$(STRIPPER) $(STRIPPERFLAGS) $@

$(OBJECTS): $(BUILDDIR)/%.o: $(PROJECT_SRC_PATH)/%.cc
	@echo -e 'Compiling: ' $<
	$(CXX) -c $(CXXFLAGS) $(LLVM_CXX_FLAGS) $(PROJECT_INC) $(CLANG_INCLUDES) $< -o $@

clean:
	rm -f $(BUILDDIR)/*o $(PROJECT_BIN_PATH)/$(ELFNAME)
