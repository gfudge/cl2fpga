\documentclass[10pt,a4paper,final]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage[left=2cm,right=2cm,top=2cm,bottom=2cm]{geometry}

\title{Kernel Rewriter Developer Documentation}
\author{Gregory Fudge}

\begin{document}

\maketitle

\section{Introduction}

\paragraph{Purpose} The following documentation intends to describe the operations and purpose of the Kernel Rewriter and related components. It intends to be detailed enough to provide an overview of the software and its structure.

\paragraph{Scope} Only key elements of the software will be described in any significant detail; a Developer is expected to be able to refer to the source code and understand the mechanics of how it works -  this document will mostly focus on what the software does. An intermediate understanding of C++ is required to understand the code base, which includes somewhat heavy use of C++11 and some C++14 features, such as Smart Pointers, Range-based For loops, use of the \texttt{auto} keyword and heavy use of the STL. An understanding of the Clang API is highly recommended, however, this is not essential as most Clang use is documented and searchable using the Clang doxygen documentation.\footnote{http://clang.llvm.org/doxygen/}

\section{Program Structure}
\subsection{Entry Point}
\paragraph{Main.cc} Calling the main function will attempt to read in the file provided as \texttt{argv[1]} as a \texttt{std::string}. Successful reading in of the input source will call the \texttt{KernelRewriteAction} class, inherited by the \texttt{clang::ASTFrontendAction} class\footnote{http://clang.llvm.org/doxygen/classclang\_1\_1ASTFrontendAction.html}. This call is made using Clang's tooling as an entry point, with the \texttt{clang::tooling::runToolOnCode()} function\footnote{http://clang.llvm.org/doxygen/namespaceclang\_1\_1tooling.html}.

\subsection{FrontendAction}
\paragraph{KernelRewriteAction} This class is used to invoke the AST Consumer that is \texttt{RewriteKernel}. It also generates the output file for the tool, by changing the extension from \texttt{.cl} to \texttt{.c} and appending \texttt{-hls} to mark that the file contains code that has been transformed. For example, the file \texttt{vector\_addition.cl} will generate a transformed source file named \texttt{vector\_addition-hls.c}.

\paragraph{Output Source File} The output of the tool is a transformed source file. The data for this object is stored as an \texttt{llvm::raw\_ostream} filestream which is passed into the AST consumer itself, where the source transformations will be written to this stream.

\subsection{RewriteKernel}
\paragraph{ASTConsumer}\footnote{http://clang.llvm.org/doxygen/classclang\_1\_1ASTConsumer.html} The vast majority of processing is done in this class, which accepts the source as a string and will write the result to file as discussed previously. Three key methods in this file are triggered by the tooling machinery:

\begin{itemize}
\item AST Consumer Initialisation - \texttt{Initialize()}
\item AST Top Level Declaration Handling - \texttt{HandleTopLevelDecl()}
\item Translation Unit Completion - \texttt{HandleTranslationUnit()}
\end{itemize}

\paragraph{\texttt{Initialize()}} This method is invoked shortly after construction of the class. This method is used as a hook from the constructor to perform initial housekeeping operations required during transformation operations, such as setting the Source Manager, Language Options and Preprocessor.

\paragraph{\texttt{HandleTopLevelDecl()}} We hook the AST traversal here- this is where transformation begins. For a single file, we process any function declarations by iterating over the range of function declarations the AST parser has detected. The walking machinery here will determine where and what to transform in the syntax tree.

\paragraph{\texttt{HandleTranslationUnit()}} Called after \texttt{HandleTopLevelDecl()} has completed, this method injects the final code strings and will copy the buffer of rewritten source code to the file buffer, flushing it to the file. The file will only be written to if there are any code transformations waiting to be written in the rewrite buffer structure.

\section{Target Hardware Design Structure}

\paragraph{Workgroup Unit} The design unit of a single transformed kernel is that of a single OpenCL Workgroup. Each synthesised Workgroup will have control of its own processing and memory of items in that group.

\subsection{Thread Control}
Basic thread control is managed by AXI4-Lite Slave interfaces, allowing the host to control operations on the device by passing the addresses of host memory to be operated on. And interrupt is used to signal to the host when operations have completed.

\subsection{Data Transfer}
Bulk data transfers are managed by an AXI4 Master bus interface which allows the Thread Block to negotiate burst memory transfers from host memory.

\subsection{Local Memory (BRAM)}
Data transferred in bulk from the host to device is stored in local BRAM storage, with one block of BRAM for each parameter passed, each block the size of the Workgroup.

\section{Abstract Syntax Tree (AST) Parsing}

\paragraph{Top Level Function Declarations} The method \texttt{HandleTopLevelDecl()} is called by Clang with an argument of \texttt{clang::DeclGroupRef}\footnote{http://clang.llvm.org/doxygen/classclang\_1\_1DeclGroupRef.html}, which can be thought of as a list of Function Declarations. It is assumed that there will only be one OpenCL Kernel in a single source file, however, this is not a limitation of the tool as it will transform any Kernel that it finds within the specified source file.

\subsection{AST Traversal Methods}

\paragraph{Checking the Function Declaration} The only requirements of a declaration to be transformed are that it is actually a \texttt{clang::FunctionDecl}\footnote{http://clang.llvm.org/doxygen/classclang\_1\_1FunctionDecl.html}, tested by dynamically casting the object as one. It must also have a kernel attribute (specifically: \texttt{clang::OpenCLKernelAttr}\footnote{https://www.crest.iu.edu/projects/conceptcpp/docs/html-ext/classclang\_1\_1OpenCLKernelAttr.html}) and not be an implicit declaration. This is to avoid transformation of non-OpenCL code and transforming implicit declarations that may have no body, both of which we have no interest in.

\paragraph{RewriteKernelFunction} Passing the target Kernel Function to this method is the only way the tool will rewrite a source file. This method checks the Function has a body, which is represented in the syntax tree as a \texttt{clang::CompoundStmt}\footnote{http://clang.llvm.org/doxygen/classclang\_1\_1CompoundStmt.html}. The tool must rewrite both the declaration of the function as well as its body in order to produce correct code, which will pass the portions of code to their respective rewriters.

\paragraph{RewriteKernelDeclaration} 

\paragraph{RewriteKernelStmt} The body of the kernel is initially passed to this function to be rewritten, however, this method is also called by other portions of code, including recursively by itself. A simple function, it merely searches through the syntax tree through all child statements checking for \texttt{clang::Expr}\footnote{http://clang.llvm.org/doxygen/classclang\_1\_1Expr.html} statements. When it finds an Expr statement, it will pass it to the \texttt{RewriteKernelExpr} method, then continuing to recursively search the tree until the all child nodes have been searched.

\paragraph{RewriteKernelExpr} Currently, the expression checker will rewrite a number of \texttt{clang::CallExpr}\footnote{http://clang.llvm.org/doxygen/classclang\_1\_1CallExpr.html} nodes of interest, typically those responsible for thread ID resolution. These are handled by replacing the expression with a variable containing the thread ID within the current scope. These methods will check the dimension that was requested in the function call and resolve that specific dimension.

\section{Code Generation}

\subsection{AXI4 Interfaces}

\paragraph{AXI4 Master Interface} Each variable declaration in the kernel that passes a reference (pointer) will generate an AXI4 Master bus interface that will enable the local workgroup to initiate bus transactions with the host.

\paragraph{AXI4 Slave Control Interface} As well as a master interface, each variable declaration has a AXI4-lite slave interface generated as part of the control bus bundle. A return interface is also generated in the control bundle to signal the thread block having finished operations.

\paragraph{Thread Block Control} Control of the Thread Block is maintained by use of the Xilinx provided driver interface which uses the AXI4-Lite interface to write the base address of each memory block to the Control Block. On completing processing of a Workgroup, a thread block will trigger the host device using an AXI4-Lite interface interrupt into the host.

\subsection{Thread Block Local Storage}

\paragraph{Local BRAM Arrays} Each Thread Block is synthesised with an amount of Block RAM for local storage. This enables larger chunks of memory to be copied from the host into local storage for faster processing of proximately stored local items. This code is generated by the method \texttt{RewriteKernel::createBRAMString()} which analyses each of the extracted kernel parameters and generates block ram instantiations, which are represented as local arrays in C.

\paragraph{Host-Device Memory Interface} Transfer of the Workgroup data to local BRAM storage is performed by using the Xilinx implementation of the \texttt{memcpy()}\footnote{http://www.xilinx.com/support/documentation/sw\_manuals/xilinx2014\_1/ug902-vivado-high-level-synthesis.pdf} function, which is able to perform burst transactions of the AXI4 master interface. These transactions will perform bursts at the start of operation of the kernel and will write data back to the host after the Thread Block has completed an operation.

\paragraph{Host to Device Transfer} Copying data from Host memory is performed in the manner described above. \texttt{RewriteKernel::createMemcpyReadString()} generates this code based on information stored about the kernel parameters extracted when parsing the AST.

\paragraph{Device to Host Transfer} The method \texttt{RewriteKernel::createMemcpyWriteString()} works in the same manner as it's sister method, however copying data back to the host memory this time.

\subsection{Local Thread Block (Workgroup)}

\paragraph{Workgroup Dispatch} An OpenCL Workgroup is dispatched to the thread block by passing base addresses of the data and other required arguments to be used and triggering the start of operation with the Xilinx driver. The block will generate an interrupt in the host when the program reaches a return statement, allowing the host to handle completion as appropriate. As data it written back to Host memory by the Thread Block, therefore the driver layer must ensure the result data is captured once the Thread Block has signalled completion.

\end{document}